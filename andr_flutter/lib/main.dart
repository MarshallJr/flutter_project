import 'dart:convert';
import 'dart:developer' as developer;
import 'SecondPage.dart';
import 'env.dart' as env;
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'model/Venues.dart';

void main() => runApp(MyApp());

/// This Widget is the main application widget.
class MyApp extends StatelessWidget {
  static const String _title = '♥ Kinder ♥';
  final _formKey = GlobalKey<FormState>();
  var isLoading = false;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      theme: ThemeData(
        primaryColor: Colors.deepPurple,
      ),
      home: MyCustomForm(),
    );
  }
}

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();
  List<Venue> _suggestions = new List();
  TextEditingController _location = TextEditingController();
  var isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: <Widget>[
          Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  controller: _location,
                  decoration: InputDecoration(labelText: 'Enter a location'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter a location';
                    }
                    return null;
                  },
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: RaisedButton(
                    onPressed: () async {
                      // Validate returns true if the form is valid, or false
                      // otherwise.
                      if (_formKey.currentState.validate()) {
                        print(_location.text);
                        _suggestions = await _fetchVenues(_location.text);
                        for (Venue v in _suggestions) {
                          print(v.name);
                        }
                        print(_suggestions.length);
                      }
                    },
                    child: Text('Submit'),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: isLoading
                ? Center(
              child: CircularProgressIndicator(),
            )
            : ListView.builder(
              itemCount: _suggestions.length,
              itemBuilder: (BuildContext context, int index) {
                print(index);
                if(_suggestions[index].categories.isEmpty){
                  return ListTile(
                    contentPadding: EdgeInsets.all(10.0),
                    title: new Text(_suggestions[index].name),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SecondRoute(venue: _suggestions[index]),
                        ),
                      );
                    },
                  );
                }
                return ListTile(
                  contentPadding: EdgeInsets.all(10.0),
                  title: new Text(_suggestions[index].name),
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage(
                        _suggestions[index].categories[0].icon.prefix +
                            "44" +
                            _suggestions[index].categories[0].icon.suffix),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => SecondRoute(venue: _suggestions[index]),
                      ),
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Future<List<Venue>> _fetchVenues(String location) async {
    setState(() {
      isLoading = true;
    });
    final baseUrl = "https://api.foursquare.com/v2/venues/search";
    final response = await http.get(baseUrl +
        "?client_id=" +
        env.clientId +
        "&client_secret=" +
        env.secretId +
        "&v=20181231&near=" +
        location);

    if (response.statusCode == 200) {
      JsonResponseVenues jsonAllDataResponse =
          JsonResponseVenues.fromJson(json.decode(response.body));
      List<Venue> venues_Sugges = jsonAllDataResponse.venues.getVenues();
      setState(() {
        isLoading = false;
      });
      return venues_Sugges;
    } else {
      print("error");
      throw Exception('Failed to load venues');
    }
  }
}


