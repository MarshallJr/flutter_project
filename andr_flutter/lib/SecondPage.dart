import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'Map.dart';
import 'env.dart' as env;
import 'model/Photo.dart';
import 'model/TipsVenue.dart';
import 'model/Venues.dart';

class SecondRoute extends StatelessWidget {
  final Venue venue;

  SecondRoute({Key key, @required this.venue}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: this.venue.name,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: DetailsPage(venue: this.venue),
    );
  }
}

class DetailsPage extends StatefulWidget {
  final Venue venue;

  DetailsPage({Key key, this.venue}) : super(key: key);

  @override
  State<DetailsPage> createState() {
    return DetailPageWidget(this.venue);
  }
}

class DetailPageWidget extends State<DetailsPage> {
  final Venue venue;

  DetailPageWidget(this.venue);

  List<Iteme> imgList = new List();
  List<Item> cmtList = new List();
  var isLoadingImages = false;
  var isLoadingComments = false;

  @override
  void initState() {
    super.initState();
    _fetchImages();
    _fetchComments();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(venue.name),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                // Stroked text as border.
                Text(
                  venue.name,
                  style: TextStyle(
                    fontSize: 40,
                    foreground: Paint()
                      ..style = PaintingStyle.stroke
                      ..strokeWidth = 6
                      ..color = Colors.blue[700],
                  ),
                ),
                // Solid text as fill.
                Text(
                  venue.name,
                  style: TextStyle(
                    fontSize: 40,
                    color: Colors.grey[300],
                  ),
                ),
              ],
            ),
            Text(
              venue.location.getFormattedAddress(),
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Divider(),
            Expanded(
              child: Column(
                children: <Widget>[
                  isLoadingImages
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : ListView.builder(
                          itemCount: imgList.length,
                          shrinkWrap: true,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                                height: 500,
                                child: Image.network(imgList[index].prefix +
                                    "400x500" +
                                    imgList[index].suffix));
                          },
                        ),
                ],
              ),
            ),
            Divider(),
            Container(
              child: Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      // Stroked text as border.
                      Text(
                        "Quelques commentaires : ",
                        style: TextStyle(
                          fontSize: 25,
                          foreground: Paint()
                            ..style = PaintingStyle.stroke
                            ..strokeWidth = 6
                            ..color = Colors.deepPurple[700],
                        ),
                      ),
                      // Solid text as fill.
                      Text(
                        "Quelques commentaires : \n",
                        style: TextStyle(
                          fontSize: 25,
                          color: Colors.grey[300],
                        ),
                      ),
                    ],
                  ),
                  isLoadingComments
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : ListView.builder(
                          shrinkWrap: true,
                          itemCount: cmtList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              child: Center(
                                child: Text(
                                  cmtList[index].user.firstName +
                                      " " +
                                      cmtList[index].user.lastName +
                                      ": " +
                                      cmtList[index].text +
                                      "\n",
                                  style: TextStyle(
                                      fontStyle: FontStyle.italic,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            );
                          },
                        ),
                ],
              ),
            ),
            Divider(),
            RaisedButton(
                color: Colors.teal,
                shape: Border(bottom: BorderSide(color: Colors.black)),
                child: Text("Trouvez l'endroit sur la carte"),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Maps(venue: venue),
                      ));
                }),
          ],
        ),
      ),
    );
  }

  ///
  /// Appel Future pour récupérer les informations sur la venue que l'utilisateur a choisi (Image + commentaire)
  ///

  Future<List<Iteme>> _fetchImages() async {
    setState(() {
      isLoadingImages = true;
    });

    String venueId = this.venue.id;
    print(this.venue.id);
    final baseUrl = "https://api.foursquare.com/v2/venues/" +
        venueId +
        "/photos?&client_id=" +
        env.clientId +
        "&client_secret=" +
        env.secretId +
        "&v=20181231";

    final response = await http.get(baseUrl);

    if (response.statusCode == 200) {
      JsonResponseImg jsonDataImages =
          JsonResponseImg.fromJson(json.decode(response.body));
      this.imgList = jsonDataImages.response.photos.getItems();
      setState(() {
        isLoadingImages = false;
      });
      return this.imgList;
    } else {
      throw Exception('Probleme avec le chargement des images ... ');
    }
  }

  Future<List<Item>> _fetchComments() async {
    setState(() {
      isLoadingComments = true;
    });

    String venueId = this.venue.id;
    final baseUrl = "https://api.foursquare.com/v2/venues/" +
        venueId +
        "/tips?&client_id=" +
        env.clientId +
        "&client_secret=" +
        env.secretId +
        "&v=20181231&sort=recent&limit=7";
    final response = await http.get(baseUrl);

    if (response.statusCode == 200) {
      JsonResponseTips commment =
          JsonResponseTips.fromJson(json.decode(response.body));

      this.cmtList = commment.response.tips.getCom();

      setState(() {
        isLoadingComments = false;
      });
      return this.cmtList;
    } else {
      throw Exception('Problème avec le chargement des commentaires');
    }
  }
}
