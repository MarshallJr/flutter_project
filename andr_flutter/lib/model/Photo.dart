import 'dart:convert';

JsonResponseImg JsonResponseImgFromJson(String str) =>
    JsonResponseImg.fromJson(json.decode(str));


class JsonResponseImg {
  Meta meta;
  Response response;

  JsonResponseImg({
    this.meta,
    this.response,
  });

  factory JsonResponseImg.fromJson(Map<String, dynamic> json) => JsonResponseImg(
    meta: Meta.fromJson(json["meta"]),
    response: Response.fromJson(json["response"]),
  );

  Map<String, dynamic> toJson() => {
    "meta": meta.toJson(),
    "response": response.toJson(),
  };
}

class Meta {
  int code;
  String requestId;

  Meta({
    this.code,
    this.requestId,
  });

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
    code: json["code"],
    requestId: json["requestId"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "requestId": requestId,
  };
}

class Response {
  Photos photos;

  Response({
    this.photos,
  });

  factory Response.fromJson(Map<String, dynamic> json) => Response(
    photos: Photos.fromJson(json["photos"]),
  );

  Map<String, dynamic> toJson() => {
    "photos": photos.toJson(),
  };
}

class Photos {
  int count;
  List<Iteme> items;
  int dupesRemoved;

  Photos({
    this.count,
    this.items,
    this.dupesRemoved,
  });

  factory Photos.fromJson(Map<String, dynamic> json) => Photos(
    count: json["count"],
    items: List<Iteme>.from(json["items"].map((x) => Iteme.fromJson(x))),
    dupesRemoved: json["dupesRemoved"],
  );

  Map<String, dynamic> toJson() => {
    "count": count,
    "items": List<dynamic>.from(items.map((x) => x.toJson())),
    "dupesRemoved": dupesRemoved,
  };

  List<Iteme> getItems() {
    return this.items;
  }
}

class Iteme {
  String id;
  int createdAt;
  Source source;
  String prefix;
  String suffix;
  int width;
  int height;
  User user;
  Checkin checkin;
  String visibility;

  Iteme({
    this.id,
    this.createdAt,
    this.source,
    this.prefix,
    this.suffix,
    this.width,
    this.height,
    this.user,
    this.checkin,
    this.visibility,
  });

  factory Iteme.fromJson(Map<String, dynamic> json) => Iteme(
    id: json["id"],
    createdAt: json["createdAt"],
    source: Source.fromJson(json["source"]),
    prefix: json["prefix"],
    suffix: json["suffix"],
    width: json["width"],
    height: json["height"],
    user: User.fromJson(json["user"]),
    checkin: Checkin.fromJson(json["checkin"]),
    visibility: json["visibility"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "createdAt": createdAt,
    "source": source.toJson(),
    "prefix": prefix,
    "suffix": suffix,
    "width": width,
    "height": height,
    "user": user.toJson(),
    "checkin": checkin.toJson(),
    "visibility": visibility,
  };
}

class Checkin {
  String id;
  int createdAt;
  String type;
  int timeZoneOffset;

  Checkin({
    this.id,
    this.createdAt,
    this.type,
    this.timeZoneOffset,
  });

  factory Checkin.fromJson(Map<String, dynamic> json) => Checkin(
    id: json["id"],
    createdAt: json["createdAt"],
    type: json["type"],
    timeZoneOffset: json["timeZoneOffset"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "createdAt": createdAt,
    "type": type,
    "timeZoneOffset": timeZoneOffset,
  };
}

class Source {
  String name;
  String url;

  Source({
    this.name,
    this.url,
  });

  factory Source.fromJson(Map<String, dynamic> json) => Source(
    name: json["name"],
    url: json["url"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "url": url,
  };
}

class User {
  String id;
  String firstName;
  String lastName;
  String gender;
  Photo photo;

  User({
    this.id,
    this.firstName,
    this.lastName,
    this.gender,
    this.photo,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"],
    firstName: json["firstName"],
    lastName: json["lastName"],
    gender: json["gender"],
    photo: Photo.fromJson(json["photo"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "firstName": firstName,
    "lastName": lastName,
    "gender": gender,
    "photo": photo.toJson(),
  };
}

class Photo {
  String prefix;
  String suffix;

  Photo({
    this.prefix,
    this.suffix,
  });

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
    prefix: json["prefix"],
    suffix: json["suffix"],
  );

  Map<String, dynamic> toJson() => {
    "prefix": prefix,
    "suffix": suffix,
  };
}
