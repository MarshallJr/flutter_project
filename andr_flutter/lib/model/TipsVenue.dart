import 'dart:convert';

JsonResponseTips jonResponseTips(String str) => JsonResponseTips.fromJson(json.decode(str));

class JsonResponseTips {
  Meta meta;
  Response response;

  JsonResponseTips({
    this.meta,
    this.response,
  });

  factory JsonResponseTips.fromJson(Map<String, dynamic> json) => JsonResponseTips(
    meta: Meta.fromJson(json["meta"]),
    response: Response.fromJson(json["response"]),
  );

  Map<String, dynamic> toJson() => {
    "meta": meta.toJson(),
    "response": response.toJson(),
  };
}

class Meta {
  int code;
  String requestId;

  Meta({
    this.code,
    this.requestId,
  });

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
    code: json["code"],
    requestId: json["requestId"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "requestId": requestId,
  };
}

class Response {
  Tips tips;

  Response({
    this.tips,
  });

  factory Response.fromJson(Map<String, dynamic> json) => Response(
    tips: Tips.fromJson(json["tips"]),
  );

  Map<String, dynamic> toJson() => {
    "tips": tips.toJson(),
  };
}

class Tips {
  int count;
  List<Item> items;

  Tips({
    this.count,
    this.items,
  });

  factory Tips.fromJson(Map<String, dynamic> json) => Tips(
    count: json["count"],
    items: List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "count": count,
    "items": List<dynamic>.from(items.map((x) => x.toJson())),
  };
  List<Item> getCom(){
    return this.items;
  }

}

class Item {
  String id;
  int createdAt;
  String text;
  String type;
  String canonicalUrl;
  Likes likes;
  bool logView;
  int agreeCount;
  int disagreeCount;
  Todo todo;
  User user;
  String authorInteractionType;

  Item({
    this.id,
    this.createdAt,
    this.text,
    this.type,
    this.canonicalUrl,
    this.likes,
    this.logView,
    this.agreeCount,
    this.disagreeCount,
    this.todo,
    this.user,
    this.authorInteractionType,
  });

  factory Item.fromJson(Map<String, dynamic> json) => Item(
    id: json["id"],
    createdAt: json["createdAt"],
    text: json["text"],
    type: json["type"],
    canonicalUrl: json["canonicalUrl"],
    likes: Likes.fromJson(json["likes"]),
    logView: json["logView"],
    agreeCount: json["agreeCount"],
    disagreeCount: json["disagreeCount"],
    todo: Todo.fromJson(json["todo"]),
    user: User.fromJson(json["user"]),
    authorInteractionType: json["authorInteractionType"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "createdAt": createdAt,
    "text": text,
    "type": type,
    "canonicalUrl": canonicalUrl,
    "likes": likes.toJson(),
    "logView": logView,
    "agreeCount": agreeCount,
    "disagreeCount": disagreeCount,
    "todo": todo.toJson(),
    "user": user.toJson(),
    "authorInteractionType": authorInteractionType,
  };
}

class Likes {
  int count;
  List<dynamic> groups;

  Likes({
    this.count,
    this.groups,
  });

  factory Likes.fromJson(Map<String, dynamic> json) => Likes(
    count: json["count"],
    groups: List<dynamic>.from(json["groups"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "count": count,
    "groups": List<dynamic>.from(groups.map((x) => x)),
  };
}

class Todo {
  int count;

  Todo({
    this.count,
  });

  factory Todo.fromJson(Map<String, dynamic> json) => Todo(
    count: json["count"],
  );

  Map<String, dynamic> toJson() => {
    "count": count,
  };
}

class User {
  String id;
  String firstName;
  String lastName;
  String gender;
  Photo photo;

  User({
    this.id,
    this.firstName,
    this.lastName,
    this.gender,
    this.photo,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"],
    firstName: json["firstName"],
    lastName: json["lastName"],
    gender: json["gender"],
    photo: Photo.fromJson(json["photo"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "firstName": firstName,
    "lastName": lastName,
    "gender": gender,
    "photo": photo.toJson(),
  };
}

class Photo {
  String prefix;
  String suffix;

  Photo({
    this.prefix,
    this.suffix,
  });

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
    prefix: json["prefix"],
    suffix: json["suffix"],
  );

  Map<String, dynamic> toJson() => {
    "prefix": prefix,
    "suffix": suffix,
  };
}
